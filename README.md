# Kao1ADPPlay
Utility to play ADPCM music from Kao the Kangaroo 1 directly.

## Building
You need to have C compiler and CMake or Meson installed. Then in terminal while being in project directory run:
```console
mkdir build
cd build
cmake ..
make -j$(nproc)
```

## Usage
See --help argument for informations.

## Project status
What works:
- ADPCM file playing using system sound libraries

Supported operating systems/platforms and sound drivers:
- Microsoft Windows (WASAPI, WinMM)
- Linux (ALSA, OSS, sndio)
- FreeBSD (OSS)
- OpenBSD (sndio)
- SerenityOS (SerenityOS AudioServer)
- output to stdout
