// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "winmm_sound.h"

#include <windows.h>
#include <stdio.h>
#include <mmsystem.h>

static struct{
	HWAVEOUT wave;
	const char* deviceName;
} instance;

static UINT WinMMSound_deviceNameToID(const char* device){
	if(strcmp(device,"default")==0)
		return WAVE_MAPPER;
	int deviceCount=waveOutGetNumDevs();
	WAVEOUTCAPS caps;
	for(int i=0; i<deviceCount; i++){
		waveOutGetDevCapsA(i,&caps,sizeof(WAVEOUTCAPS));
		if(strcmp(caps.szPname,device)==0)
			return (UINT)i;
	}
	return WAVE_MAPPER;
}
void WinMMSound_init(const char* device){
	device=device?device:"default";

	WAVEFORMATEX wavFormat={0};
	wavFormat.wFormatTag=WAVE_FORMAT_PCM;
	wavFormat.nChannels=2;
	wavFormat.nSamplesPerSec=44100;
	wavFormat.wBitsPerSample=16;
	wavFormat.nBlockAlign=wavFormat.nChannels*wavFormat.wBitsPerSample/8;
	wavFormat.nAvgBytesPerSec=wavFormat.nSamplesPerSec*wavFormat.nBlockAlign;
	if(waveOutOpen(&instance.wave,WinMMSound_deviceNameToID(device),&wavFormat,(DWORD_PTR)0,(DWORD_PTR)0,CALLBACK_NULL)!=MMSYSERR_NOERROR){
		printf("(Error) [WinMM] Failed to open device %s\n",device);
		exit(1);
	}

	if(waveOutSetVolume(instance.wave,0xFFFFFFFF)!=MMSYSERR_NOERROR){
		printf("(Error) [WinMM] Failed to set volume\n");
		exit(1);
	}

	instance.deviceName=device;
}
void WinMMSound_delete(void){}
void WinMMSound_play(const char* data,unsigned size){
	WAVEHDR header={0};
	header.lpData=(CHAR*)data;
	header.dwBufferLength=size;
	if(waveOutPrepareHeader(instance.wave,&header,sizeof(WAVEHDR))!=MMSYSERR_NOERROR){
		printf("(Warning) [WinMM] Failed to prepare header\n");
		return;
	}

	if(waveOutWrite(instance.wave,&header,sizeof(WAVEHDR))!=MMSYSERR_NOERROR){
		printf("(Warning) [WinMM] Failed to write wave data\n");
		return;
	}
	while(!(header.dwFlags&WHDR_DONE));
}
const char* WinMMSound_getDriverName(void){
	return "winmm";
}
const char* WinMMSound_getDeviceName(void){
	return instance.deviceName;
}
