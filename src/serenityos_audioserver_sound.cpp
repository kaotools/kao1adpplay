// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <AK/Types.h>
#include <AK/FixedArray.h>
#include <LibAudio/ConnectionToServer.h>
#include <LibAudio/Resampler.h>
#include <LibAudio/Sample.h>
#include <LibCore/EventLoop.h>
#include <LibDSP/Track.h>
#include <limits>

// Some parts of this code were inspired by SerenityOS Piano application

static struct{
	RefPtr<Audio::ConnectionToServer> audioClient;
	Optional<Audio::ResampleHelper<DSP::Sample>> resampler;
	Core::EventLoop* eventLoop; // not used in this program but OS requires to create one
} instance;

extern "C"{
void SerenityOSAudioServerSound_init(void){
	instance.eventLoop=new Core::EventLoop;
	instance.audioClient=Audio::ConnectionToServer::try_create().release_value_but_fixme_should_propagate_errors();
	instance.resampler=Audio::ResampleHelper<DSP::Sample>(44100,instance.audioClient->get_sample_rate());
}
void SerenityOSAudioServerSound_delete(void){
	delete instance.eventLoop;
}
void SerenityOSAudioServerSound_play(const char* data,unsigned size){
	FixedArray<DSP::Sample> buffer=FixedArray<DSP::Sample>::must_create_but_fixme_should_propagate_errors(size/sizeof(int16_t)/2);
	for(unsigned i=0; i<size/4; i++){
		buffer[i].left=(float)(*((int16_t*)&data[i*4]))/(float)std::numeric_limits<int16_t>::max();
		buffer[i].right=(float)(*((int16_t*)&data[i*4+2]))/(float)std::numeric_limits<int16_t>::max();
	}
	Vector<Audio::Sample> resampledBuffer=instance.resampler->resample(buffer);
	(void)instance.audioClient->async_enqueue(resampledBuffer);
	while(instance.audioClient->remaining_samples()>0);
}
const char* SerenityOSAudioServerSound_getDriverName(void){
	return "serenityos_audioserver";
}
const char* SerenityOSAudioServerSound_getDeviceName(void){
	return "default";
}
}
