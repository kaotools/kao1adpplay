// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "oss_sound.h"

#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/soundcard.h>
#include <sys/ioctl.h>
#include <fcntl.h>

static struct{
	int dspFd;
} instance;

static void OSSSound_error(const char* msg,bool shouldCloseFd){
	printf("(Error) [OSS] %s\n",msg);
	if(shouldCloseFd)
		OSSSound_delete();
	exit(1);
}
void OSSSound_init(void){
	if((instance.dspFd=open("/dev/dsp",O_WRONLY))<0)
		OSSSound_error("Failed to open '/dev/dsp'",false);

	int format=AFMT_S16_LE;
	if(ioctl(instance.dspFd,SNDCTL_DSP_SETFMT,&format)<0)
		OSSSound_error("Failed to set format to AFMT_S16_LE",true);

	int channels=2;
	if(ioctl(instance.dspFd,SNDCTL_DSP_STEREO,&channels)<0)
		OSSSound_error("Failed to set channel count",true);

	int sampleRate=44100;
	if(ioctl(instance.dspFd,SNDCTL_DSP_SPEED,&sampleRate)<0)
		OSSSound_error("Failed to set sample rate",true);
}
void OSSSound_delete(void){
	close(instance.dspFd);
}
void OSSSound_play(const char* data,unsigned size){
	unsigned left=size;
	unsigned offset=0;
	while(left>0){
		int written=write(instance.dspFd,data+offset,left);
		if(written<0){
			printf("(Warning) [OSS] Failed to write samples\n");
			break;
		}
		left-=(unsigned)written;
		offset+=(unsigned)written;
	}
}
const char* OSSSound_getDriverName(void){
	return "oss";
}
const char* OSSSound_getDeviceName(void){
	return "dsp";
}
