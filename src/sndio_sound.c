// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "sndio_sound.h"

#include <stdio.h>
#include <stdbool.h>
#include <sndio.h>

#define SNDIO_PLAY_CHUNK_SIZE 176400 // 44100*channels*sizeof(int16_t)

static struct{
	struct sio_hdl* hdl;
	const char* device;
} instance;

static void SndioSound_error(const char* msg,bool shouldCloseHdl){
	printf("(Error) [sndio] %s\n",msg);
	if(shouldCloseHdl)
		sio_close(instance.hdl);
}
void SndioSound_init(const char* device){
	instance.device=device?device:"default";
	if((instance.hdl=sio_open(device,SIO_PLAY,0))==0)
		SndioSound_error("Failed to open sndio handle",false);

	struct sio_par params;
	sio_initpar(&params);
	params.rate=44100;
	params.pchan=2;
	params.bits=16;
	params.le=SIO_LE_NATIVE;
	params.sig=1;
	params.round=params.rate/8;
	params.appbufsz=params.round*2;
	if(!sio_setpar(instance.hdl,&params))
		SndioSound_error("Failed to set parameters",true);

	if(!sio_start(instance.hdl))
		SndioSound_error("Failed to start sndio",true);
}
void SndioSound_delete(void){
	sio_stop(instance.hdl);
	sio_close(instance.hdl);
}
void SndioSound_play(const char* data,unsigned size){
	unsigned offset=0,left=size,retryCount=0;
	while(left>0){
		unsigned toWrite=left>SNDIO_PLAY_CHUNK_SIZE?SNDIO_PLAY_CHUNK_SIZE:left;
		unsigned written=(unsigned)sio_write(instance.hdl,data+offset,toWrite);
		if(retryCount>50){
			printf("(Warning) [sndio] Retry count exceeded\n");
			break;
		}
		if(written==0){
			printf("(Warning) [sndio] Failed to write samples, retrying: %d\n",retryCount);
			retryCount++;
			continue;
		}
		retryCount=0;
		left-=written;
		offset+=written;
 	}
}
const char* SndioSound_getDriverName(void){
	return "sndio";
}
const char* SndioSound_getDeviceName(void){
	return instance.device;
}
