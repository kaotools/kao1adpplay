// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "alsa_sound.h"

#include <alsa/asoundlib.h>

static struct{
	snd_pcm_t* pcm;
	snd_pcm_hw_params_t* params;
	snd_pcm_info_t* info;
	snd_pcm_uframes_t frames;
} instance;

void AlsaSound_init(const char* device){
	device=device?device:"default";
	if(snd_pcm_open(&instance.pcm,device,SND_PCM_STREAM_PLAYBACK,0)<0){
		printf("(Error) [AlsaSound] Failed to open PCM device '%s'\n",device);
		exit(1);
	}

	snd_pcm_hw_params_alloca(&instance.params);
	snd_pcm_hw_params_any(instance.pcm,instance.params);
	if(snd_pcm_hw_params_set_access(instance.pcm,instance.params,SND_PCM_ACCESS_RW_INTERLEAVED)<0){
		printf("(Error) [AlsaSound] Failed to set interleaved access\n");
		exit(1);
	}
	if(snd_pcm_hw_params_set_format(instance.pcm,instance.params,SND_PCM_FORMAT_S16_LE)<0){
		printf("(Error) [AlsaSound] Failed to set signed 16-bit LE format");
		exit(1);
	}
	if(snd_pcm_hw_params_set_channels(instance.pcm,instance.params,2)<0){
		printf("(Error) [AlsaSound] Failed to set 2 channels\n");
		exit(1);
	}
	int freq=44100;
	if(snd_pcm_hw_params_set_rate_near(instance.pcm,instance.params,&freq,0)<0){
		printf("(Error) [AlsaSound] Failed to set 44100 Hz sample rate\n");
		exit(1);
	}
	if(snd_pcm_hw_params(instance.pcm,instance.params)<0){
		printf("(Error) [AlsaSound] Failed to apply params\n");
		exit(1);
	}

	snd_pcm_info_alloca(&instance.info);
	snd_pcm_info(instance.pcm,instance.info);

	snd_pcm_hw_params_get_period_size(instance.params,&instance.frames,0);

}
void AlsaSound_delete(void){
	snd_pcm_drain(instance.pcm);
	snd_pcm_close(instance.pcm);
}
void AlsaSound_play(const char* data,unsigned size){
	unsigned offset=0;
	char buffer[instance.frames*4];

	while(offset<size){
		while((snd_pcm_uframes_t)snd_pcm_avail_update(instance.pcm)<instance.frames);
	
		memset(buffer,0,instance.frames*4);
		unsigned toMix=size-offset<instance.frames*4?size-offset:instance.frames*4;
		memcpy(buffer,data+offset,toMix);
	
		if(snd_pcm_writei(instance.pcm,buffer,instance.frames)==-EPIPE)
			snd_pcm_prepare(instance.pcm);
		offset+=toMix;
	}
}
const char* AlsaSound_getDriverName(){
	return "alsa";
}
const char* AlsaSound_getDeviceName(){
	return snd_pcm_info_get_name(instance.info);
}
