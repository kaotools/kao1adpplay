// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef KAO1ADPPLAY_SOUND_H
#define KAO1ADPPLAY_SOUND_H

void Sound_init(const char* driver,const char* device);
void Sound_delete(void);
void Sound_play(const char* data,unsigned size);
const char* Sound_getDriverName(void);
const char* Sound_getDeviceName(void);
const char** Sound_getAvailableSoundDrivers(unsigned* length);

#endif
