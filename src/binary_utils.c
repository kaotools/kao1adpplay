// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "binary_utils.h"

#include <stdbool.h>

static bool isLittleEndian(){
	unsigned x=0x76543210;
	char* c=(char*)&x;
	return *c==0x10;
}
uint16_t bytesToUint16(char* data){
	uint16_t output;
	if(isLittleEndian()){
		((char*)&output)[0]=data[0];
		((char*)&output)[1]=data[1];
	}
	else{
		((char*)&output)[1]=data[0];
		((char*)&output)[0]=data[1];
	}
	return output;
}
void uint16ToBytes(uint16_t data,char* output){
	if(isLittleEndian()){
		output[0]=((char*)&data)[0];
		output[1]=((char*)&data)[1];
	}
	else{
		output[1]=((char*)&data)[0];
		output[0]=((char*)&data)[1];
	}
}
uint32_t bytesToUint32(char* data){
	uint32_t output;
	if(isLittleEndian()){
		((char*)&output)[0]=data[0];
		((char*)&output)[1]=data[1];
		((char*)&output)[2]=data[2];
		((char*)&output)[3]=data[3];
	}
	else{
		((char*)&output)[3]=data[0];
		((char*)&output)[2]=data[1];
		((char*)&output)[1]=data[2];
		((char*)&output)[0]=data[3];
	}
	return output;
}
void uint32ToBytes(uint32_t data,char* output){
	if(isLittleEndian()){
		output[0]=((char*)&data)[0];
		output[1]=((char*)&data)[1];
		output[2]=((char*)&data)[2];
		output[3]=((char*)&data)[3];
	}
	else{
		output[3]=((char*)&data)[0];
		output[2]=((char*)&data)[1];
		output[1]=((char*)&data)[2];
		output[0]=((char*)&data)[3];
	}
}
void byteArrayToUint16Array(char* data,uint16_t* output,unsigned length){
	for(unsigned i=0; i<length/sizeof(uint16_t); i++){
		output[i]=bytesToUint16(&data[i*sizeof(uint16_t)]);
	}
}
void uint16ArrayToByteArray(uint16_t* data,char* output,unsigned length){
	for(unsigned i=0; i<length; i++){
		uint16ToBytes(data[i],&output[i*sizeof(uint16_t)]);
	}
}