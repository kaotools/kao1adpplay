// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "wasapi_sound.h"

#define INITGUID
#include <windows.h>
#include <mmdeviceapi.h>
#include <audioclient.h>
#include <functiondiscoverykeys_devpkey.h>
#include <stdio.h>

static struct{
       	IAudioClient* player;
	IAudioRenderClient* playerRender;
	UINT32 frames;
	char* deviceName;
} instance;

static void WASAPISound_error(const char* message){
	printf("(Error) [WASAPISound] %s\n");
	exit(1);
}
static IMMDevice* WASAPISound_deviceNameToIMMDevice(const char* name,IMMDeviceEnumerator* enumerator){
	IMMDevice* output=0;

	IMMDeviceCollection* collection;
	if(enumerator->lpVtbl->EnumAudioEndpoints(enumerator,eRender,DEVICE_STATE_ACTIVE,&collection)!=S_OK)
		WASAPISound_error("Failed to enumerate devices");
	UINT collectionCount;
	if(collection->lpVtbl->GetCount(collection,&collectionCount)!=S_OK)
		WASAPISound_error("Failed to get device collection");
	for(UINT i=0; i<collectionCount; i++){
		IMMDevice* device;
		if(collection->lpVtbl->Item(collection,i,&device)!=S_OK)
			WASAPISound_error("Failed to retrieve collection element");
		IPropertyStore* properties;
		if(device->lpVtbl->OpenPropertyStore(device,STGM_READ,&properties)!=S_OK)
			WASAPISound_error("Failed to open property store");
		PROPVARIANT prop;
		PropVariantInit(&prop);
		if(properties->lpVtbl->GetValue(properties,&PKEY_Device_FriendlyName,&prop)!=S_OK)
			WASAPISound_error("Failed to get device friendly name");

		char cstr[wcslen(prop.pwszVal)+1];
		cstr[wcslen(prop.pwszVal)]=0;
		wcstombs(cstr,prop.pwszVal,wcslen(prop.pwszVal));
		if(strcmp(cstr,name)==0){
			output=device;
			PropVariantClear(&prop);
			properties->lpVtbl->Release(properties);
			break;
		}

		PropVariantClear(&prop);
		properties->lpVtbl->Release(properties);
		device->lpVtbl->Release(device);
	}
	collection->lpVtbl->Release(collection);

	return output;
}
static char* WASAPISound_getIMMDeviceName(IMMDevice* device){
	IPropertyStore* properties;
	if(device->lpVtbl->OpenPropertyStore(device,STGM_READ,&properties)!=S_OK)
		WASAPISound_error("Failed to open property store");
	PROPVARIANT prop;
	PropVariantInit(&prop);
	if(properties->lpVtbl->GetValue(properties,&PKEY_Device_FriendlyName,&prop)!=S_OK)
		WASAPISound_error("Failed to get device friendly name");

	char* cstr=malloc(wcslen(prop.pwszVal)+1);
	cstr[wcslen(prop.pwszVal)]=0;
	wcstombs(cstr,prop.pwszVal,wcslen(prop.pwszVal));
	PropVariantClear(&prop);
       	properties->lpVtbl->Release(properties);

	return cstr;
}
void WASAPISound_init(const char* device){
	device=device?device:"default";
	if(CoInitializeEx(0,COINIT_SPEED_OVER_MEMORY)!=S_OK)
		WASAPISound_error("Failed to initialize");

	IMMDeviceEnumerator* enumerator;
	if(CoCreateInstance(&CLSID_MMDeviceEnumerator,0,CLSCTX_ALL,&IID_IMMDeviceEnumerator,(LPVOID*)&enumerator)!=S_OK)
		WASAPISound_error("Failed to create device enumerator");

       	IMMDevice* playDevice;
	if(strcmp(device,"default")==0){
		if(enumerator->lpVtbl->GetDefaultAudioEndpoint(enumerator,eRender,eConsole,&playDevice)!=S_OK)
			WASAPISound_error("Failed to get default device");
	}
	else{
		playDevice=WASAPISound_deviceNameToIMMDevice(device,enumerator);
		if(!playDevice)
			WASAPISound_error("Failed to enumerate device");
	}
       	enumerator->lpVtbl->Release(enumerator);

	if(playDevice->lpVtbl->Activate(playDevice,&IID_IAudioClient,CLSCTX_ALL,0,(LPVOID*)&instance.player)!=S_OK)
		WASAPISound_error("Failed to activate audio device");
	instance.deviceName=WASAPISound_getIMMDeviceName(playDevice);
       	playDevice->lpVtbl->Release(playDevice);


	WAVEFORMATEX playFormat={0};
	playFormat.wFormatTag=WAVE_FORMAT_PCM;
	playFormat.nChannels=2;
	playFormat.nSamplesPerSec=44100;
	playFormat.wBitsPerSample=16;
	playFormat.nBlockAlign=playFormat.nChannels*playFormat.wBitsPerSample/8;
	playFormat.nAvgBytesPerSec=playFormat.nSamplesPerSec*playFormat.nBlockAlign;
	DWORD initFlags=AUDCLNT_STREAMFLAGS_RATEADJUST|AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM|AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY;
	if(instance.player->lpVtbl->Initialize(instance.player,AUDCLNT_SHAREMODE_SHARED,initFlags,10000000,0,&playFormat,0)!=S_OK)
		WASAPISound_error("Failed to initialize audio client");

	if(instance.player->lpVtbl->GetBufferSize(instance.player,&instance.frames)!=S_OK)
		WASAPISound_error("Failed to get buffer size");

	if(instance.player->lpVtbl->GetService(instance.player,&IID_IAudioRenderClient,(LPVOID*)&instance.playerRender)!=S_OK)
		WASAPISound_error("Failed to create audio render client");

	instance.player->lpVtbl->Start(instance.player);
}
void WASAPISound_delete(void){
	instance.player->lpVtbl->Stop(instance.player);
	instance.playerRender->lpVtbl->Release(instance.playerRender);
	instance.player->lpVtbl->Release(instance.player);
	free(instance.deviceName);
}
void WASAPISound_play(const char* data,unsigned size){
	unsigned offset=0;
	while(offset<size){
		UINT32 padding;
		if(instance.player->lpVtbl->GetCurrentPadding(instance.player,&padding)!=S_OK)
			WASAPISound_error("Failed to get padding");

		UINT32 soundBufferLatency=instance.frames/50;
		UINT32 framesToWrite=soundBufferLatency-padding;

		UINT8* buffer;
		if(instance.playerRender->lpVtbl->GetBuffer(instance.playerRender,framesToWrite,&buffer)!=S_OK)
			WASAPISound_error("Failed to get sound buffer");

		unsigned toMix=size-offset<framesToWrite*4?size-offset:framesToWrite*4;
		for(UINT32 i=0; i<framesToWrite*4; i++){
			if(i<=toMix)
				*(buffer++)=data[offset+i];
			else
				*(buffer++)=0;
		}
		offset+=toMix;
		if(instance.playerRender->lpVtbl->ReleaseBuffer(instance.playerRender,framesToWrite,0)!=S_OK)
			WASAPISound_error("Failed to free sound buffer");
	}
}
const char* WASAPISound_getDriverName(void){
	return "wasapi";
}
const char* WASAPISound_getDeviceName(void){
	return instance.deviceName;
}
