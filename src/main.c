// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "sound.h"
#include "binary_utils.h"

void printUsage(char** args){
	printf("KAO1ADPPlay 1.1.0 - tool to play ADPCM audio from Kao the Kangaroo directly\n");
	printf("Usage: %s [arguments] <file>\n",args[0]);
	printf("--driver\tSet sound driver. Available sound drivers:");
	unsigned availableSoundDriverCount;
	const char** soundDrivers=Sound_getAvailableSoundDrivers(&availableSoundDriverCount);
	for(unsigned i=0; i<availableSoundDriverCount; i++){
		printf(" %s",soundDrivers[i]);
	}
	printf("\n");
	printf("--device\tSet playback device\n");
	printf("--times\t\tRepeat playing sound provided amount of times\n");
	printf("--stdout-output\tWrite output to stdout instead of playing it\n");
	printf("--help\t\tDisplay this message\n");
}
int16_t decodeADPCMSample(int8_t nibble,int* stepIndex,int* step,int* predictor){
	const int indexTable[]={
		-1, -1, -1, -1, 2, 4, 6, 8,
		-1, -1, -1, -1, 2, 4, 6, 8
	};
	const int stepTable[]={ 
		7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 
		19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 
		50, 55, 60, 66, 73, 80, 88, 97, 107, 118, 
		130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
		337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
		876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 
		2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
		5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899, 
		15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767 
	};
	(*stepIndex)+=indexTable[(uint8_t)nibble];
	(*stepIndex)=(*stepIndex)>88?88:(*stepIndex);
	(*stepIndex)=(*stepIndex)<0?0:(*stepIndex);
	int8_t delta=nibble&7;
	int diff=(*step)>>3;
	if(delta&4)
		diff+=(*step);
	if(delta&2)
		diff+=((*step)>>1);
	if(delta&1)
		diff+=((*step)>>2);
	if(nibble&8)
		(*predictor)-=diff;
	else
		(*predictor)+=diff;
	(*predictor)=(*predictor)>32767?32767:(*predictor);
	(*predictor)=(*predictor)<-32768?-32768:(*predictor);
	(*step)=stepTable[(*stepIndex)];
	return *predictor;
}
int16_t* decodeADPCM(char* data,unsigned int size){
	int stepIndex=0;
	int step=7;
	int predictor=0;
	int16_t* output=malloc(size*4);
	for(unsigned int i=0; i<size; i++){
		int8_t n1=data[i]&0x0F;
		int8_t n2=(data[i]&0xF0)>>4;
		output[i*2]=decodeADPCMSample(n1,&stepIndex,&step,&predictor);
		output[i*2+1]=decodeADPCMSample(n2,&stepIndex,&step,&predictor);
	}
	return output;
}
int main(int argc,char** args){
	const char* argInput=0;
	int argTimes=1;
	const char* argDriver=0;
	const char* argDevice=0;
	bool argStdoutOutput=false;
	for(int i=1; i<argc; i++){
		if(strcmp(args[i],"--driver")==0){
			if(i++>=argc){
				printf("(Error) [Main] --driver argument expects driver name\n");
				exit(1);
			}
			argDriver=args[i];
		}
		else if(strcmp(args[i],"--device")==0){
			if(i++>=argc){
				printf("(Error) [Main] --device argument expects device name\n");
				exit(1);
			}
			argDevice=args[i];
		}
		else if(strcmp(args[i],"--times")==0){
			if(i++>=argc){
				printf("(Error) [Main] --times argument expects play times count\n");
				exit(1);
			}
			argTimes=atoi(args[i]);
		}
		else if(strcmp(args[i],"--help")==0){
			printUsage(args);
			exit(0);
		}
		else if(strcmp(args[i],"--stdout-output")==0)
			argStdoutOutput=true;
		else
			argInput=args[i];
	}
	if(!argInput){
		printf("(Error) [Main] No input file specified\n");
		exit(1);
	}

	FILE* musicFile=fopen(argInput,"rb");
	if(!musicFile){
		printf("(Error) [Main] Failed to open file %s\n",args[1]);
		return 1;
	}
	printf("(Log) [Main] Decompressing ADPCM...");
	fflush(stdout);
	fseek(musicFile,0,SEEK_END);
	long musicFileSize=ftell(musicFile);
	rewind(musicFile);
	char* adp=malloc(musicFileSize);
	fread(adp,musicFileSize,1,musicFile);
	fclose(musicFile);
	if(strncmp(adp,"tadp",4)!=0){
		free(adp);
		printf(" invalid file header\n");
		return 1;
	}
	uint32_t channelSize=bytesToUint32(&adp[4]);
	int16_t* channel1=decodeADPCM((char*)(adp+8),musicFileSize-8-channelSize);
	int16_t* channel2=decodeADPCM((char*)(adp+8+channelSize),musicFileSize-8-channelSize);

	int16_t* channel=malloc((musicFileSize-8)*2*sizeof(int16_t));
	for(unsigned i=0; i<musicFileSize-8-channelSize; i++){
		channel[i*4]=channel1[i*2];
		channel[i*4+1]=channel2[i*2];
		channel[i*4+2]=channel1[i*2+1];
		channel[i*4+3]=channel2[i*2+1];
	}
	printf(" done\n");
	free(channel1);
	free(channel2);
	free(adp);
	if(!argStdoutOutput){
		Sound_init(argDriver,argDevice);
		printf("(Log) [Main] Playing '%s', sound driver '%s' using device '%s'\n",argInput,Sound_getDriverName(),Sound_getDeviceName());
		unsigned trackLengthMinutes=floorf((float)channelSize*2/44100.0f/60.0f);
		unsigned trackLengthSeconds=channelSize*2/44100%60;
		printf("(Log) [Main] Info: track length %02d:%02d, size %d B, size after decompression %d B\n",trackLengthMinutes,trackLengthSeconds,channelSize*2,channelSize*8);
		fflush(stdout);
	}

	while(argTimes!=0){
		if(argStdoutOutput){
			unsigned left=channelSize*8;
			unsigned offset=0;
			while(left>0){
				int written=fwrite(channel+offset,left,1,stdout);
				if(written<0){
					fprintf(stderr,"(Warning) [Main] Failed to write data to stdout\n");
					break;
				}
				left-=(unsigned)written;
				offset+=(unsigned)written;
			}
		}
		else
			Sound_play((const char*)channel,channelSize*8);
		argTimes--;
	}

	if(!argStdoutOutput)
		Sound_delete();
	free(channel);
	return 0;
}
