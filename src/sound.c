// KAO1ADPPlay
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "sound.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#if defined(SOUND_ALSA)
#include "alsa_sound.h"
#endif
#if defined(SOUND_OSS)
#include "oss_sound.h"
#endif
#if defined(SOUND_SNDIO)
#include "sndio_sound.h"
#endif
#if defined(SOUND_WASAPI)
#include "wasapi_sound.h"
#endif
#if defined(SOUND_WINMM)
#include "winmm_sound.h"
#endif
#if defined(SOUND_SERENITYOS_AUDIOSERVER)
#include "serenityos_audioserver_sound.h"
#endif

static const char* supportedSoundDrivers[]={
	#if defined(SOUND_ALSA)
	"alsa",
	#endif
	#if defined(SOUND_OSS)
	"oss",
	#endif
	#if defined(SOUND_SNDIO)
	"sndio",
	#endif
	#if defined(SOUND_WASAPI)
	"wasapi",
	#endif
	#if defined(SOUND_WINMM)
	"winmm",
	#endif
	#if defined(SOUND_SERENITYOS_AUDIOSERVER)
	"serenityos_audioserver",
	#endif
};

static struct{
	void (*delete)(void);
	void (*play)(const char*,unsigned);
	const char* (*getDriverName)(void);
	const char* (*getDeviceName)(void);
} instance;

#if defined(SOUND_ALSA)
static void Sound_initAlsa(const char* device){
	AlsaSound_init(device);
	instance.delete=AlsaSound_delete;
	instance.play=AlsaSound_play;
	instance.getDriverName=AlsaSound_getDriverName;
	instance.getDeviceName=AlsaSound_getDeviceName;
}
#endif
#if defined(SOUND_OSS)
static void Sound_initOSS(){
	OSSSound_init();
	instance.delete=OSSSound_delete;
	instance.play=OSSSound_play;
	instance.getDriverName=OSSSound_getDriverName;
	instance.getDeviceName=OSSSound_getDeviceName;
}
#endif
#if defined(SOUND_SNDIO)
static void Sound_initSndio(const char* device){
	SndioSound_init(device);
	instance.delete=SndioSound_delete;
	instance.play=SndioSound_play;
	instance.getDriverName=SndioSound_getDriverName;
	instance.getDeviceName=SndioSound_getDeviceName;
}
#endif
#if defined(SOUND_WASAPI)
static void Sound_initWASAPI(const char* device){
	WASAPISound_init(device);
	instance.delete=WASAPISound_delete;
	instance.play=WASAPISound_play;
	instance.getDriverName=WASAPISound_getDriverName;
	instance.getDeviceName=WASAPISound_getDeviceName;
}
#endif
#if defined(SOUND_WINMM)
static void Sound_initWinMM(const char* device){
	WinMMSound_init(device);
	instance.delete=WinMMSound_delete;
	instance.play=WinMMSound_play;
	instance.getDriverName=WinMMSound_getDriverName;
	instance.getDeviceName=WinMMSound_getDeviceName;
}
#endif
#if defined(SOUND_SERENITYOS_AUDIOSERVER)
static void Sound_initSerenityOSAudioServer(void){
	SerenityOSAudioServerSound_init();
	instance.delete=SerenityOSAudioServerSound_delete;
	instance.play=SerenityOSAudioServerSound_play;
	instance.getDriverName=SerenityOSAudioServerSound_getDriverName;
	instance.getDeviceName=SerenityOSAudioServerSound_getDeviceName;
}
#endif
void Sound_init(const char* driver,const char* device){
	const char* used=0;
	unsigned soundDriverCount=sizeof(supportedSoundDrivers)/sizeof(const char*);
	if(soundDriverCount==0){
		printf("(Error) [Sound] No sound driver is compiled\n");
		exit(1);
	}
	else if(soundDriverCount==1)
		used=supportedSoundDrivers[0];
	else if(driver)
		used=driver;
	else{
		#if defined(__linux__)
		#if defined(SOUND_ALSA)
		used="alsa";
		#elif defined(SOUND_OSS)
		used="oss";
		#elif defined(SOUND_SNDIO)
		used="sndio";
		#endif
		#elif defined(_WIN32)
		#if defined(SOUND_WASAPI)
		used="wasapi";
		#elif defined(SOUND_WINMM)
		used="winmm";
		#endif
		#elif defined(__openbsd__)
		#if defined(SOUND_SNDIO)
		used="sndio";
		#endif
		#endif
		#if defined(SOUND_SERENITYOS_AUDIOSERVER)
		used="serenityos_audioserver";
		#endif
	}

	bool soundInitialized=false;
	if(strcmp(used,"alsa")==0){
		#if defined(SOUND_ALSA)
		Sound_initAlsa(device);
		soundInitialized=true;
		#else
		printf("(Error) [Sound] ALSA support is not compiled\n");
		exit(1);
		#endif
	}
	else if(strcmp(used,"oss")==0){
		 #if defined(SOUND_OSS)
		 Sound_initOSS();
		 soundInitialized=true;
		 #else
		 printf("(Error) [Sound] OSS support is not compiled\n");
		 exit(1);
		 #endif
	}
	else if(strcmp(used,"sndio")==0){
		#if defined(SOUND_SNDIO)
		Sound_initSndio(device);
		soundInitialized=true;
		#else
		printf("(Error) [Sound] sndio support is not compiled\n");
		exit(1);
		#endif
	}
	else if(strcmp(used,"wasapi")==0){
		#if defined(SOUND_WASAPI)
		Sound_initWASAPI(device);
		soundInitialized=true;
		#else
		printf("(Error) [Sound] WASAPI support is not compiled\n");
		exit(1);
		#endif
	}
	else if(strcmp(used,"winmm")==0){
		#if defined(SOUND_WINMM)
		Sound_initWinMM(device);
		soundInitialized=true;
		#else
		printf("(Error) [Sound] WinMM support is not compiled\n");
		exit(1);
		#endif
	}
	else if(strcmp(used,"serenityos_audioserver")==0){
		#if defined(SOUND_SERENITYOS_AUDIOSERVER)
		Sound_initSerenityOSAudioServer();
		soundInitialized=true;
		#else
		printf("(Error) [Sound] SerenityOS AudioServer support is not compiled\n");
		exit(1);
		#endif
	}
	if(!soundInitialized){
		printf("(Error) [Sound] Platform is unsupported\n");
		exit(1);
	}
}
void Sound_delete(){
	instance.delete();
}
void Sound_play(const char* data,unsigned size){
	instance.play(data,size);
}
const char* Sound_getDriverName(void){
	return instance.getDriverName();
}
const char* Sound_getDeviceName(void){
	return instance.getDeviceName();
}
const char** Sound_getAvailableSoundDrivers(unsigned* length){
	*length=sizeof(supportedSoundDrivers)/sizeof(const char*);
	return supportedSoundDrivers;
}
